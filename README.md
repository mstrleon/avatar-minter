<img src="server/output/1.png" height="270">


# NFT Avatar Minter

This app generates avatar, based on random DNA, 
then uploads image to IPFS and mints to the blockchain.


## Features

- Works on any modern browser
- Based on <a href="https://www.youtube.com/c/HashLipsNFT" target="blank">Hashlips NFT tutorial</a> 
- All pic generation is performed on the server
- Avatar DNA is also randomly generated
- Pic uploads to IPFS via Infura
- Minting to any Ethereum-based blockchain network (can switch in code)
- Displays all this kind of NFTs, registered on the Blockchain.
- Uses Metamask as a wallet



## Web application demo:

https://ava-minter.herokuapp.com/


## Used by

<a href="https://inite.io" target="blank">
<img src="server/public/inite.jpg" height="60" alt="Inite.Io">
</a>


## Author

- [Leonid Anufriev](https://www.gitlab.com/mstrleon)


## Tech Stack

**Client:** React

**Blockchain** Solidity, Infura, Truffle, Ethereum
 
**Server:** Node


