const fs = require("fs")
const myArgs = process.argv.slice(2)
const { createCanvas, loadImage } = require("canvas")
const {layers, width, height} = require("./input/config.js")

const dirname = require('./util/path')
console.log(dirname)

const canvas = createCanvas(width, height)
const ctx = canvas.getContext("2d")
const edition = myArgs.length>0 ? Number(myArgs[0]) : 1

var metadata = []
var attributes = []
var hash = []
var decodedHash = []

var LastRandIndex  = {}// object for selected indexes in groups

const chooseIndex = (_layer) => {
  const randIndex = 
    _layer.groupId &&  _layer.groupId === LastRandIndex.groupId ?
    LastRandIndex.lastId  :
    Math.floor(Math.random()*_layer.elements.length)
  
    if (_layer.groupId) { // we have group
    LastRandIndex = { groupId: _layer.groupId,  lastId: randIndex}
  }
  return randIndex;
}


const addMetadata = (_edition) =>  {
  let dateTime = Date.now()
  let tempMetadata = {
    hash: hash.join(""),
    decodedHash: decodedHash,
    edition: _edition,
    date: dateTime,
    attributes: attributes
  }
  metadata.push(tempMetadata)
  //clearing 
  attributes = []
  hash = []
  decodedHash = []
}

const addAttributes = (_element, _layer) => {
  let tempAttr = {
    id: _element.id,
    layer: _layer.name,
    name: _element.name,
    rarity: _element.rarity
  }
  attributes.push(tempAttr)
  hash.push(_layer.id)
  hash.push(_element.id)
  decodedHash.push({[_layer.id]: _element.id})
}

const saveLayer = (_canvas, _edition) => {
  fs.writeFileSync(`${dirname}/public/${_edition}.png`, _canvas.toBuffer("image/png"))
  //console.log("image created");
}

// randomness directly when draw
const drawLayer = async (_layer, _edition) =>  {
  //setting up randomness
  // check if we already selected ID for this group
  
  let randIndex = chooseIndex(_layer) // wether rand or previos of the group
  
  let element = _layer.elements[randIndex]
  addAttributes(element, _layer)
  
  const image = await loadImage(`${_layer.location}${element.fileName}`)
  ctx.drawImage(
     image, 
     _layer.position.left, 
     _layer.position.top, 
     _layer.size.width, 
     _layer.size.height
     )
  // console.log(
  //    `Created the ${_layer.name} layer and chose element ${element.name}`
  //    );
  
  await saveLayer(canvas, _edition)
}


const generateAvatar = async () => {
    layers.forEach (layer => {
       drawLayer(layer, 1);
    })
    addMetadata(1)
  
  fs.readFileSync(`${dirname}/public/_metadata.json`, (err, data) => {
    if (err) throw err
    fs.writeFileSync(`${dirname}/public/_metadata.json`, JSON.stringify(metadata))
  })
  
}

module.exports = generateAvatar
