const path = require('path');

const express = require('express');
//const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3001;

const app = express();

// const cors = require('cors');
// app.use(cors({
//     origin: 'http://localhost:3000'
// }));

//app.set('view engine', 'ejs')
//app.set('views', 'views')

const appRoutes = require('./routes/routes');


app.use(express.static(path.join(__dirname, 'public')));
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static( 'client/build'));

app.use(appRoutes);

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
})