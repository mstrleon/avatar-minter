const path = require('path');

const express = require('express');

const rootDir = require('../util/path');

const router = express.Router();

const generate = require("../functions")

const PORT = process.env.PORT || 3001;


router.get('/get-avatar',(req,res,next) => {
  console.log('requested avatar generation')
  res.setHeader('Content-type', 'application/json')
  generate()
  res.end(JSON.stringify({ avatarURL: `https://ava-minter.herokuapp.com/1.png` }));
} )

// All other GET requests not handled before will return our React app
router.get('*', (req, res) => {
  console.log(path.resolve(__dirname, '../../client/build', 'index.html'))
  res.sendFile(path.resolve(__dirname, '../../client/build', 'index.html'));
});


module.exports = router;
