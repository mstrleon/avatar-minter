const fs = require ("fs")
const width = 700
const height = 700
const dir = __dirname
const rarity = [
  { key: "", val: "original"},
  { key: "_r", val: "rare"},
  { key: "_sr", val: "super rare"}
]

const addRarity = (_str) => {
  let itemRarity
  rarity.forEach ((r) => {
    if (_str.includes(r.key)) {
      itemRarity  = r.val;
    }
  })
  return itemRarity
}

const cleanName = (_str) => {
  let name = _str.slice(0,-4)
  rarity.forEach((r) => {
    name = name = name.replace(r.key, "")
  })
  return name
}

const getElements = (path) => {
  return fs
  .readdirSync(path)
  .filter((item) => !/(^|\/)\.[^\/\.]/g.test(item))
  .map((item, index) => {
    return {
      id: index +1,
      name: cleanName(item),
      fileName: item,
      rarity: addRarity(item)
    }
  })
}

// grouped parts should go straight one by one, not mix with other group

const layers = [
  {
    id: 1,
    groupId: 1,
    name: "Arm left",
    location: `${dir}/arm_left/`,
    elements: getElements(`${dir}/arm_left/`),
    size: {width: 200, height: 200},
       position: {
      top: 390,
      left: 345
    }
  },
  {
    id: 2,
    groupId: 1,
    name: "Arm right",
    pairedId: 1,
    location: `${dir}/arm_right/`,
    elements: getElements(`${dir}/arm_right/`),
    size: {width: 200, height: 200},
    position: {
      top: 390,
      left: 155
    },

  },
  {
    id: 3,
    groupId: 2,
    name: "Hand left",
    location: `${dir}/hand_left/`,
    elements: getElements(`${dir}/hand_left/`),
    size: {width: 200, height: 200},
    position: {
      top: 425,
      left: 135
    },
  },
  {
    id: 4,
    name: "Hand right",
    groupId: 2,
    pairedId: 3,
    location: `${dir}/hand_right/`,
    elements: getElements(`${dir}/hand_right/`),
    size: {width: 200, height: 200},
    position: {
      top: 425,
      left: 360
    }
  },
  {
    id: 5,
    name: "Leg left",
    groupId: 3,
    location: `${dir}/leg_left/`,
    elements: getElements(`${dir}/leg_left/`),
    size: {width: 200, height: 200},
    position: {
      top: 525,
      left: 200
    },
  },
  {
    id: 6,
    name: "Leg right",
    groupId: 3,
    location: `${dir}/leg_right/`,
    elements: getElements(`${dir}/leg_right/`),
    size: {width: 200, height: 200},
    position: {
      top: 525,
      left: 305
    }
  },
  {
    id: 7,
    name: "Body",
    location: `${dir}/body/`,
    elements: getElements(`${dir}/body/`),
    size: {width: 400, height: 400},
    position: {
      top: 310,
      left: 150
    }
  },
  {
    id: 8,
    name: "Head",
    location: `${dir}/head/`,
    elements: getElements(`${dir}/head/`),
    size: {width: 500, height: 500},
    position: {
      top: 25,
      left: 100
    }
  },
  {
    id: 9,
    name: "Face",
    location: `${dir}/face/`,
    elements: getElements(`${dir}/face/`),
    size: {width: 400, height: 300},
    position: {
      top: 220,
      left: 150
    }
  },
];

//console.log(layers)

module.exports = {layers, width, height}