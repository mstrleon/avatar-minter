/* eslint-disable jest/valid-describe */
const {assert} = require ("chai")
const SmartContract = artifacts.require("./AvatarContract.sol")

require("chai").use(require("chai-as-promised")).should()


contract( "AvatarContract", (accounts) => {
  let smartContract

  before(async () => {
    smartContract = await SmartContract.deployed()
  })

  describe("deployment", async() => {
    it("deploys successfully", async() => {
      const address = await smartContract.address;
      assert.notEqual(address, "")
      assert.notEqual(address, 0x0)
    } )
  })
  describe("minting", async() => {
    it ("minted succesfully", async () => {
      const uri = "https://example.com"
      await smartContract.mint(accounts[0], uri ) // account of current app
      const  tokenUri = await smartContract.tokenURI(0)
      const balanceOfOwner = await smartContract.balanceOf(accounts[0])
      assert.equal(tokenUri, uri)
      assert.equal(balanceOfOwner, 1)
    })
  })


})
