const initialState = {
  loading: false,
  name: "",
  error: false,
  errorMsg: "",
  blockchainName: "",
  allTokens: [],
  avatarURL: "",
};

const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case "CHECK_DATA_REQUEST":
      return {
        ...initialState,
        loading: true,
      };
    case "CHECK_DATA_SUCCESS":
      return {
        ...initialState,
        loading: false,
        name: action.payload.name,
        allTokens: action.payload.tokens,
        blockchainName: action.payload.blockchainName
      };
    case "CHECK_DATA_FAILED":
      return {
        ...initialState,
        loading: false,
        error: true,
        errorMsg: action.payload,
      };
    case "AVATAR_FETCH_SUCCESS":  
      return {
        ...state,
        avatarURL: action.payload.avatarURL
      }
    case "AVATAR_CLEAR":  
      return {
        ...state,
        avatarURL: ""
      }
    default:
      return state;
  }
};

export default dataReducer;
