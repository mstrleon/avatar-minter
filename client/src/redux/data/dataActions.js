// log
import store from "../store";
import axios from "axios"
import config from "../../config.js"

const fetchDataRequest = () => {
  return {
    type: "CHECK_DATA_REQUEST",
  };
};

const fetchDataSuccess = (payload) => {
  return {
    type: "CHECK_DATA_SUCCESS",
    payload: payload,
  };
};

const fetchDataFailed = (payload) => {
  return {
    type: "CHECK_DATA_FAILED",
    payload: payload,
  };
};

export const fetchData = (account) => {
  return async (dispatch) => {
    dispatch(fetchDataRequest());
    try {
      let name = await store
        .getState()
        .blockchain.avatarContract.methods.name()
        .call();
      let tokens = await store
        .getState()
        .blockchain.avatarContract.methods.getAllTokens()
        .call();
      let blockchainName = await store
        .getState()
        .blockchain.web3.eth.net.getNetworkType();

      dispatch(
        fetchDataSuccess({
          name,
          tokens,
          blockchainName
        })
      );
    } catch (err) {
      console.log(err);
      dispatch(fetchDataFailed("Could not load data from contract."));
    }
  };
};

const avatarSuccess = (payload) => {
  return {
    type: "AVATAR_FETCH_SUCCESS",
    payload: payload,
  };
}; 

export const getAvatar = () => {
  return async (dispatch) => {
    const res = await axios.get(config.generatorURL+"get-avatar")
    const avatarURL = await res.data.avatarURL

    console.log(res.data.avatarURL)
    dispatch(
      avatarSuccess({
        avatarURL
      })
    ); 
  }
}
export const clearAvatar = () => {
  return {
    type: "AVATAR_CLEAR",
  };
}
