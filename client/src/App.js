import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { connect } from "./redux/blockchain/blockchainActions";
import { fetchData, clearAvatar, getAvatar } from "./redux/data/dataActions";
import * as s from "./styles/globalStyles";
import styled from "styled-components";
import { create } from "ipfs-http-client";
import axios from "axios"

const ipfsClient = create("https://ipfs.infura.io:5001/api/v0")

export const StyledButton = styled.button`
  padding: 8px;
`;


function App() {
  const dispatch = useDispatch();
  const blockchain = useSelector((state) => state.blockchain);
  const data = useSelector((state) => state.data);

  const[loading, setLoading] = useState(false)
  const[status, setStatus] = useState("")
  const[mintedStatus, setMintedStatus] = useState(false)
  const[NFTs, setNFTs] = useState([])

  const ipfsBaseUrl = "https://ipfs.infura.io/ipfs/"  // can do "ipfs://lkjhHL&" style later
  const name = "Avatar name"
  const description = "IPFS minted avatar"

  console.log(NFTs)

  const mint = (_uri) => {
    setStatus(`Minting NFT to ${data.blockchainName} blockchain`)
    blockchain.avatarContract.methods.mint(blockchain.account,_uri)
    .send({from: blockchain.account})
    .once("error", (err)=> {
      console.log(err) 
      setLoading(false)
      setStatus("Error on minting avatar to blockchain")
    }).then((receipt) => {
      console.log(receipt)
      setLoading(false)
      setStatus("Successfully minted your NFT")
      setMintedStatus(true)
      dispatch(fetchData(blockchain.account))
    })
  }

  const createMetaDataAndMint = async (_name, _desc, _imgBuffer) => {
    setStatus("Uploading image to IPFS...")
    try{
      const addedImage = await ipfsClient.add(_imgBuffer)
      const tempMetaData = {
        name: _name,
        description: _desc,
        image: ipfsBaseUrl + addedImage.path
      } 
      const addeedMetaData = await ipfsClient.add(JSON.stringify(tempMetaData))
      console.log(ipfsBaseUrl + addeedMetaData.path);
      mint(ipfsBaseUrl + addeedMetaData.path)
    }
    catch (err){
      console.error(err)
      setLoading(false)
      setStatus("Error on uploading avatar")
    }
  }

  const  startMintingProcess = async () => {
    setLoading(true)
    const buffer = await getImageData()
    // here we can define name and desc, or use default for now
    createMetaDataAndMint(name, description, buffer )
  }

  const  getImageData = async () => {
    const res = await axios.get(data.avatarURL,  { responseType: 'arraybuffer' })
    const buffer = Buffer(res.data, "utf-8")
    return buffer
  }

  const fetchMetaDataForNFTs = () =>  {
    setNFTs([])
    data.allTokens.forEach((nft)=> {
      axios.get(nft.uri)
      .then(
        (res) => {
          setNFTs((prevState => [...prevState, {id: nft.id, metaData: res.data}]))
        }
      ).catch((e) => {
        console.error(e)
      })
    })
  }

  useEffect(() => {
    if (blockchain.account !== "" && blockchain.avatarContract !== null) {
      dispatch(fetchData(blockchain.account));
    }
  }, [blockchain.avatarContract, dispatch]);

  useEffect(() => {
    if ( data.avatarURL ) {
      setMintedStatus(false)
    } else {
      setStatus("")
    }
  },[data.avatarURL])

  useEffect(() => {
   fetchMetaDataForNFTs()
  },[data.allTokens])



  return (
    <s.Screen> 
      <s.Container flex={1} ai={"center"} jc={"center"}>
      <s.SpacerLarge />
        {blockchain.account === "" || blockchain.avatarContract === null ? (
         <>
          <s.TextTitle>Connect to the Blockchain</s.TextTitle>
          <s.SpacerSmall />
          <StyledButton
            onClick={(e) => {
              e.preventDefault();
              dispatch(connect());
            }}
          >
            CONNECT
          </StyledButton>
          <s.SpacerSmall />
          {blockchain.errorMsg !== "" ? (
            <s.TextDescription>{blockchain.errorMsg}</s.TextDescription>
          ) : null}
        </>
      ) : 
      data.avatarURL === "" ? 
       (
        <s.Container flex={1} ai={"center"} jc={"start"}>
          <s.TextTitle style={{ textAlign: "center" }}>
            Name of Contract: {data.name}.
          </s.TextTitle>
          <s.SpacerLarge />
          <StyledButton
            onClick={(e) => {
              e.preventDefault();
              setMintedStatus(false)
              clearAvatar()
              dispatch(getAvatar())
            }}
          >Generate Avatar</StyledButton>
        </s.Container>
      ) :
      (
        <s.Container flex={1} ai={"center"} jc={"start"}>
          <s.TextTitle style={{ textAlign: "center" }}>
            Name of Contract: {data.name}.
          </s.TextTitle>
          <s.SpacerLarge />
          <img src={data.avatarURL} alt="generated avatar" width="400" height="400"/>
          <s.SpacerLarge />
          { !mintedStatus ? (
              <StyledButton
                onClick={(e) => {
                  e.preventDefault();
                  startMintingProcess();
                }}
              >Issue NFT</StyledButton> 
           ) : (
                <StyledButton
                onClick={(e) => {
                  e.preventDefault();
                  setStatus("Getting new Avatar...")
                  dispatch(clearAvatar());
                  dispatch(getAvatar());
                }}
              >Generate anoter Avatar</StyledButton>
           )}
          <s.SpacerSmall/>
           { loading? (
            <>
              <s.TextDescription>
               loading <br/><marquee>........</marquee>
              </s.TextDescription>
            </>
           ) : null }
           { status!=="" ? (
            <>
              <s.TextDescription>
               {status}
              </s.TextDescription>
            </>
           ) : null}
           <s.SpacerLarge/>
        </s.Container>
      )
      }
       { data.loading? (
            <>
              <s.TextDescription>
               loading <br/><marquee>........</marquee>
              </s.TextDescription>
            </>
           ) :
           NFTs.map((nft, index)=> {
             return (
              <s.Container key = {index} style={{padding: 16}} flex={1} ai={"center"}>
                 <s.TextTitle>{nft.metaData.name} {nft.id}</s.TextTitle>
                <img 
                  alt="nft.metaData.name" 
                  src={nft.metaData.image}
                  width={150}
                  />
              </s.Container>    
             )
           }) }
      </s.Container>
    </s.Screen>
  );
}

export default App;
